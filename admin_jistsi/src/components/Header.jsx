import React from 'react';
import '../scss/_header.scss';
import Logo from "../assets/img/logo-header.svg";

const Header = props =>{    
    
    const {click} = props
        return (
            <header>
                <img src={Logo} alt="" />
                <button className="btn-header-seach" onClick={click}>Acessar minha sala</button>
            </header>
        )

}
export default Header;