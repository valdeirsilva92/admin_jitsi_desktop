import React from 'react';
import '../scss/_login-side.scss';
import InputLogin from '../components/Input';
import Button from '../components/ButtonDefault';


const LoginSide = props => {

    const { className, sideRef, click } = props;

        return(
            
            <div ref={sideRef} className={`container ${className}`}>
                <div className="close" onClick={click} ></div>
                <div className="content-login">
                <h1>Acessar minha sala</h1>
                    <InputLogin styleName="material-login" placeholder="Nome da sala" label="Sala" />
                    <InputLogin styleName="material-login" placeholder="Senha" label="Senha"/>
                    <div className="btn-access">
                        <Button styleName="btn-login" txtBtn="Acessar Sala"/>
                    </div>
                    <Button styleName="btn-plans" txtBtn="Precisa de mais sala? Conheça nossos planos >"/>
                </div>
            </div>
        )

};
export default LoginSide;