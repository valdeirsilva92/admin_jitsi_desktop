import React from 'react';
import '../scss/_input.scss';

const Input = props => {
const { styleName, placeholder, label } = props;
    return (
        <fieldset className={ styleName }>
            <input type="text" placeholder={placeholder} />
            <hr />
            <label htmlFor="">{label}</label>
        </fieldset>
    )

}
export default Input;