import React from 'react';
import '../scss/_search-home.scss';
import InputSearch from './Input';
import BtnSearch from './ButtonDefault';

const InputHome = () => {
   
        return (
            <div className="search-home">
                <h1>Vai participar de uma reunião?</h1>
                <InputSearch styleName="material-search" placeholder="Nome da reunião" label=""/>
                <BtnSearch styleName="btn-search" txtBtn="Entrar" />
            </div>
        )     

};
export default InputHome;