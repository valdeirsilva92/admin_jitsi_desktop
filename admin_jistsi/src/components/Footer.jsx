import React from 'react';
import IconAplleStore from "../assets/img/icon-apple-store.png";
import IconPlayStore from "../assets/img/icon-playstore.png";
import '../scss/_footer.scss'

const Footer = props => {


    return (
        <footer>
            <p>©2020 ConferenciaCorp</p>
            <div className="plataforms-app">
                <p>Conheça nosso aplicativo :)</p>
                <a href="http://www.uol.com.br" target="_blank" rel="noopener noreferrer" ><img src={IconPlayStore} alt="Play Store" /></a>
                <a href="http://www.g1.com.br" target="_blank" rel="noopener noreferrer"><img src={IconAplleStore} alt="Apple Store" /></a>
            </div>
        </footer>
    )

}
export default Footer;