import React from 'react';
import '../scss/_button.scss';

const ButtonDefault = props => {
    const { styleName, txtBtn} = props;

    return (
    
        <button className={ styleName }> { txtBtn }</button>

    )

    
}
export default ButtonDefault;
