import React from 'react';
import ButtonMenu from './ButtonMenu';
import ContentMenu from './ContentMenu';

const Config = props => {

    return (
        <>
            <ButtonMenu />
            <ContentMenu />
        </>
    )

}
export default Config;