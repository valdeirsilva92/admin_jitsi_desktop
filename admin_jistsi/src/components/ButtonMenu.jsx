import React from 'react';
import '../scss/_button-menu.scss';
import icon from '../assets/img/settings.svg';

const BtnMenu = props => {


    return (
        <div className="btn-menu">
           <img src={icon} alt=""/>
        </div>
    )
}
export default BtnMenu;