import React from 'react';
import '../scss/_menu-bar.scss'

const MenuBar = props => {

    return (
        <ul className="menu-bar">
            <li>Perfil</li>
            <li>Histórico</li>
            <li>Senha</li>
            <li>Salas</li>
            <li>Ajuda</li>
            <li>Sair</li>
        </ul>
    )

}
export default MenuBar;