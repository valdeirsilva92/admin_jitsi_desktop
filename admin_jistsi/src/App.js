import React, {useState, useRef} from 'react';
import Header from './components/Header';
import InputHeader from './components/InputHome';
import Footer from './components/Footer';
import LoginSide from './components/LoginSide';
import { Link } from 'react-router-dom'
import './App.scss';


function App() {

 
  const [dropdown, setDropdown] = useState(""); 
  const sideRef = useRef(null);

  const showDropdown = () => {
    //se clicar no botão, modal aparece
    setDropdown("show-side");
  }
  const closeDropdown = () => {
    setDropdown("show-close");
    
  };
  return (
    <div className="App">
      <Header click={showDropdown} />
      <Link to="/admin">Ir para a página Admin</Link>
      <InputHeader />
      <LoginSide  className={dropdown} sideRef={sideRef} click={closeDropdown}/>
      
      <Footer />
    </div>
  );
}

export default App;
