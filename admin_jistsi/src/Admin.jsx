import React from 'react';
import './scss/_admin.scss';
import Config from './components/Config'
import BG from '../src/assets/img/morgan.jpg'


function Admin() {
  return (
    <div className="admin">
        <img src={ BG } alt=""/>
        <Config />
    </div>
  );
}

export default Admin;